This is a Pycharm process that lets you create an AppImage of your python App 
without any AppImage packaging knowledge.  You develop your app as normal in Pycharm.
When ready to deploy goto 'Tools''*PROJECT APPS*' '1 compile to appimage' then
you can select '2 run appimage'. 

In order to be able to do this you need to create your python project from the provided
template folder.  The install.md shows you how to do this.  This involves
following these instructions step-by-step.


I SHOULD EMPASIZE THAT THIS A ONE PERSON DEVELOPMENT TEAM.
PRODUCTION USE IS NOT RECOMMENDED.
THIS CAN GIVE YOU SOME IDEAS ON USING APPIMAGES TO DEPLOY
PYTHON PROJECTS.

Thanks

Murray