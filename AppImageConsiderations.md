Appimages.
1 - Absolute paths are the enemy.  Images are loaded to /tmp
abd expaned and run from a different location on every run.
There are two important python variables APPDIR and HOME.
1 APPDIR - cotains the 'base' location of the app image
at runtime. It will contain something like:  '/tmp/.mount_cfile.pSPFUo'. So you
would reference the main.py file as: APPDIR + '/source/src/main.py'.
In the ide APPDIR will contain something like 
'/local/appi/zpy_charmbase/py-cmdline/' append 'source/src/main.py' to get the
absolute path.  APPDIR is context aware, it changes depending wether
it is running in the ide or appimage. print('APPDIR',APPFIR) in
main.py. Run it in the ide and then the appimage and see the difference.
You never need to set it.
2 - HOME is similar. It is the location of the HOME folder. It could
be something like /home/$USER or a home folder belonging to the
appimage.  See https://docs.appimage.org/user-guide/portable-mode.html

Murray

Do not use python virtual environments, they are not needed if you bundle
your app as an appimage. Jupyter notebooks and Anaconda do not work.
