INSTALLATION:

You need to get 'appdeploy.Appimage'
Run at terminal or from file manager.
It will display an 'open folder' dialog box.
Select your pycharm project folder,
   it can be empty.
Three folders will be placed there:
   _template
   _templateApps
   _templateProj

Launch pycharm.
Close all open projects.
Close pycharm and relaunch it so that
  you are at the initial screen.
Select "Open'then select /projectfolder/_templateProj.
This will create _templateProj as a pycharm project.

If this is fresh pycharm project (empty) then from the menu bar go to
"File', 'Settings...', 'Appearance & Behavior', 'System Settings'
and enter the absolute pycharm base path in 'Default Directory'

Go to 'File', 'Manage Ide Settings', 'Import Settings'
Navigate to: /projectfolder/_templateApps/PyCharmCE2020.1/settings.zip
and select 'Ok'. This will install new Tools options that will let
create customized applications.
Close any open projects. Close pycharm and relaunch it.
Select the '_templateProj' from the top left of the main screen.
This will open the project.


Now you can create your first project.
From the top menu select 'Tools', '*PROJECT APPS*', '4 create project'
A dialog box asks for the new project name, enter it (no spaces).
A new project folder with the name will be created. It will contain
'main.py, a python runtime, and support files. It is however
not yet a pycharm project. To make it a pycharm project, from the toolbar,
select "File' , 'Open...' and navigate to the newly created project,
click ok. Now it is a pycharm project (own .idea file).


Now you need to specify the python interpreter to be used in this
project. Each project has its own interpreter in:
../projectname/source/python/bin/python3

To set it is a bit detailed to navigate.
To make it active from toolbar:
'File' 'Settings...' 'Project' 'Project Interpreter' .
On thepanel at the right you will see 'Project Interpreter'
on the right you will see a +, click it then 'add...',
then select 'System Interpreter',on the right select ,,,
and finally navigate to ../projectname/source/python/bin/python3
and select it. This is the way pycharm sets it up. Not my
fault. Anyone know a more direct way?
The interpreter you want is always 'System Interpreter'

To run the project:
  In the project panel on the left navigate to 'main.py' and
select it so that it is in the edit window. From the main toolbar
select 'Run' 'Run..' then select 'main'. On subsequent runs the
toolbar run button will be active and can be used instead.

You can now start programming by placing your own code in
main.py.

When your project is complete and ready to deploy,
select 'Tools' '*PROJECT APPS*' '1 compile to appimage',
when done you can select '2 run appimage'.

==============================================================

Once installed the steps to create a project are:
Make sure an existing project is open, if not open the _templateProj
(File Open).
Select 'Tools' '*PROJECT APPS*' '4 Create Project' . Say you chose
a name of 'py_one'
select "File' 'Open' and naviagte to the py_one folder and open it 
to make it active.
Select the python interpreter for 'py_one' from 'File' 'Settings...'
'Project' 'Project Interpreter' 'then the star button on the right'
'Add...' 'System Interpreter' and navigate to .../py_one/source/python/bin/python3
and choose it as the interpreter.  Every project has its own
python interpreter.
From the project panel on the left navigate to 'main.py' and open it 
in the editor window and start coding.




